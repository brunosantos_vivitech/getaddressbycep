console.log('Loading function');
const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const db = new AWS.DynamoDB();

exports.handler = (event, context, callback) => {
    let cep = event.pathParameters.cep.toString();
    let params = {
        AttributesToGet: [
            "bairro",
            "localidade",
            "log_nu_bairro",
            "log_nu_localidade",
            "logradouro",
            "uf",
            "complemento"
        ],
        TableName: 'vivi_tech_dev_rede',
        Key: {
            cep: {
                "S": cep
            }
        }
    };
    let retornoCidade = {};

    db.getItem(params, function (err, data) {
        if (err) {
            console.log(err);
            return callback(err, null);
        } else {
            if (data.Item.localidade) {
                retornoCidade.localidade = (JSON.stringify(data.Item.localidade)).replace("\"\S\"\:", "").replace("\{\"", "").replace("\"\}", "");
                retornoCidade.log_nu_localidade = (JSON.stringify(data.Item.log_nu_localidade)).replace("\"\S\"\:", "").replace("\{\"", "").replace("\"\}", "");
                retornoCidade.uf = (JSON.stringify(data.Item.uf)).replace("\"\S\"\:", "").replace("\{\"", "").replace("\"\}", "");

                if (data.Item.logradouro) {
                    retornoCidade.logradouro = (JSON.stringify(data.Item.logradouro)).replace("\"\S\"\:", "").replace("\{\"", "").replace("\"\}", "");
                } else {
                    retornoCidade.logradouro = "";
                }

                if (data.Item.log_nu_bairro) {
                    retornoCidade.log_nu_bairro = (JSON.stringify(data.Item.log_nu_bairro)).replace("\"\S\"\:", "").replace("\{\"", "").replace("\"\}", "");
                } else {
                    retornoCidade.log_nu_bairro = "";
                }
                
                if (data.Item.bairro) {
                    retornoCidade.bairro = (JSON.stringify(data.Item.bairro)).replace("\"\S\"\:", "").replace("\{\"", "").replace("\"\}", "");
                } else {
                    retornoCidade.bairro = "";
                }

                if (data.Item.complemento) {
                    retornoCidade.complemento = (JSON.stringify(data.Item.complemento)).replace("\"\S\"\:", "");
                } else {
                    retornoCidade.complemento = "";
                }
            }

            var responseToAPI = {
                "statusCode" : 200,
                "body" : JSON.stringify(retornoCidade),
                "isBase64Encoded" : false
            };
            
            return callback(null, responseToAPI);
        }

    });
};
